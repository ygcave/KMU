#include <Servo.h>

Servo myservo;

int pos = 0;
int incomingByte = 0;

void setup() {
  Serial.begin(9600);
  myservo.attach(9);
}

void loop() {
  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();
    pos = incomingByte;
    myservo.write(pos);
    delay(15);
  }
}

