int num = 9;
float margin = 10;
int counter = num/2;
float t = 0.0;

void setup() {
  size(1000, 600);
  rectMode(CENTER);
}

void draw() {
  background(0);

  for (int i=0; i<num; i++) {
    stroke(255);
    strokeWeight(3);
    if (i == counter) {
      fill(abs(sin(t))*255);
    } else {
      noFill();
    }
    ellipse(width/(num+1)+i*(width/(num+1)), height/2, width/(num+1)-margin, width/(num+1)-margin);

    stroke(0, 255, 0);
    strokeWeight(1);
    line( width/(num+1)+i*(width/(num+1)), 0, width/(num+1)+i*(width/(num+1)), height);
  }
  //num = int(map(mouseX, 0, width, 1, 20));
  //margin = map(mouseY, 0, height, 0, width/(num+1));

  fill(255, 0, 255);
  stroke(255);
  strokeWeight(1);
  rect(width/2-100, height-100, 50, 50);
  rect(width/2+100, height-100, 50, 50);
  
  t += 0.024;
}

void mouseReleased() {
  if (mouseX > (width/2-100)-25 && mouseX < (width/2-100)+25) {
    if (mouseY > (height-100)-25 && mouseY < (height-100)+25) {
      println("left");
      counter--;
      if(counter < 0){
        counter = 0;
      }
    }
  }
  if (mouseX > (width/2+100)-25 && mouseX < (width/2+100)+25) {
    if (mouseY > (height-100)-25 && mouseY < (height-100)+25) {
      println("right");
      counter++;
      if(counter > num-1){
        counter = num-1;
      }
    }
  }
}

void keyReleased(){
  if(key == 'a'){
    println("a");
      counter--;
      if(counter < 0){
        counter = 0;
      }
  }
  if(key == 's'){
    println("s");
      counter++;
      if(counter > num-1){
        counter = num-1;
      }
  }
}