import processing.serial.*;
Serial myPort;
int val;

int x = 100;
int speedX = 5;
int dirX = 1;

void setup(){
  size(600, 600);
  background(0);
  myPort = new Serial(this, "COM4", 9600);
}

void draw(){
  background(0);
  
  fill(255);
  ellipse(x, height/2, 60, 60);
  
  x = x + dirX*speedX;
  
  if(x > width-30){
    dirX = dirX * -1;
  }
  if(x < 30){
    dirX = dirX * -1;
  }
  
  val = int(map(x, 30, width-30, 0, 180));
  myPort.write(val);
  
  speedX = int(map(mouseY, 0, height, 1, 8));
}