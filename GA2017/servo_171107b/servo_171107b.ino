#include <Servo.h>

Servo myservo;

int pos = 0;
int incomingByte = 0;

void setup() {
  Serial.begin(9600);
  myservo.attach(9);
}

void loop() {
  if (Serial.available() > 0) {
    // read the incoming byte:
    incomingByte = Serial.read();

    if(incomingByte >= '0' && incomingByte <= '9'){
       pos = (incomingByte-48)*20;
    }

    // say what you got:
    Serial.print("I received: ");
    Serial.println(pos, DEC);

    myservo.write(pos);
    delay(15);
  }
}

